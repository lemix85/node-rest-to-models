var util = require('util');

var BaseAttribute = function(name, value, source) {
    this.name = name;
    this.value = value;
    this.source = source;
};

var ValueAttribute = function(name, value, source) {
    BaseAttribute.apply(this, arguments);
};

util.inherits(ValueAttribute, BaseAttribute);

var RelationAttribute = function(name, value, model, modelInstance, relType, source) {
    this.model = model;
    this.modelInstance = modelInstance;
    this.relType = relType;
    BaseAttribute.apply(this, [name, value, source]);
};

util.inherits(RelationAttribute, BaseAttribute);

module.exports.ValueAttribute = ValueAttribute;
module.exports.RelationAttribute = RelationAttribute;