var ErrorResponse = function(instance, request, err, res) {
    this.rest2models = instance;
    this.request = request;
    this.err = err;
    this.res = res;
    this._process();
}

ErrorResponse.prototype._process = function() {
    this.err.status = this.err.status || 500;
    if(this.err.status == 500)
console.log(this.err.stack)
    if(process.env.NODE_ENV == 'production') {
        this.err.error = undefined;
        delete this.err.error;
    }

    this.res.status(this.err.status).json({
        error: this.err
    });
}

module.exports = ErrorResponse;