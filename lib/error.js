var util = require('util');

function CustomError(status, message) {
    var tmp = Error.apply(this, [message]);
    tmp.name = this.name = 'Rest2ModelsError';
    this.status = status;
    this.message = tmp.message;
    if (Error.captureStackTrace)
        Error.captureStackTrace(this, this.constructor);
}

util.inherits(CustomError, Error);

module.exports = CustomError;