var extend = require('node.extend'),
    debug = require('debug')('rest2models'),
    Request = require('./request'),
    Response = require('./response'),
    Error = require('./error'),
    ErrorResponse = require('./error-response'),
    Strategy = require('./strategy/strategy').Strategy,
    SequelizeStrategy = require('./strategy/sequelize');

var rest2models = function (app, strategy, options) {
    /*if(app instanceof Strategy) {
        options = arguments[1];
        strategy = arguments[0];
        app = require('express').Router({mergeParams: true});
    }*/
    this.app = this.router = app;
    this.strategy = strategy;
    this.options = extend({
        baseUrl: '',
        router: null,
        errorHandling: true
    }, options);

    this.options.baseUrl = this.options.baseUrl ? this.options.baseUrl.replace(/\/$/, '') : '/';

    this._init();
}

rest2models.prototype._init = function () {
    var ctx = this,
        baseUrl = this.options.baseUrl;/* + '/*';*/

    this.app.use(baseUrl, function (req, res, next) {
        debug('Begin request for ' + req.url);
        ctx._beginRequest.apply(ctx, arguments);
    });

    if (this.options.router) {
        ctx.app.use(baseUrl, this.options.router);
    }

    ctx.app.use(baseUrl, function (req, res, next) {
        debug('Finalize request for ' + req.url);
        ctx._finalizeRequest.apply(ctx, arguments);
    });

    if (ctx.options.errorHandling) {
        ctx.app.use(baseUrl, function (err, req, res, next) {
            new ErrorResponse(ctx, req.request, err, res);
        });
    }
}

rest2models.prototype._beginRequest = function (req, res, next) {
    var request = new Request(this, req);

    request.process()
        .then(function () { next(); })
        .catch(this._finalizeErrorRequest.bind(this, req, res, next));
}

rest2models.prototype._finalizeRequest = function (req, res, next) {
    req.request.finalize()
        .then(this._finalizeSuccessRequest.bind(this, req, res))
        .catch(this._finalizeErrorRequest.bind(this, req, res, next));
}

rest2models.prototype._finalizeSuccessRequest = function (req, res) {
    debug('Call _finalizeSuccessRequest');
    var request = req.request;

    if (this.options.hooks && this.options.hooks[req.request.targetModelName]
        && (this.options.hooks[req.request.targetModelName].verb == '*'
            || this.options.hooks[req.request.targetModelName].verb == req.method.toLowerCase())) {
        // apply hooks
        return this.options.hooks[req.request.targetModelName].beforeResponse(req, res).then((function(data) {
            req.request.data = data;
            new Response(this, request, res);
        }).bind(this));
    }
    
    new Response(this, request, res);
    return;
}

rest2models.prototype._finalizeErrorRequest = function (req, res, next, err) {
    next(err);
}

function createInstance(app, strategy, options) {
    return new rest2models(app, strategy, options);
};

createInstance.__proto__.Strategy = Strategy;
createInstance.__proto__.SequelizeStrategy = SequelizeStrategy;

module.exports = createInstance;