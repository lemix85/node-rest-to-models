var debug = require('debug')('rest2models'),
    q = require('q'),
    Error = require('./error'),
    extend = require('node.extend'),
    attr = require('./attributes'),
    utils = require('./utils'),
    ValueAttribute = attr.ValueAttribute,
    RelationAttribute = attr.RelationAttribute;

require('array.prototype.find');

class Request {
    constructor(instance, req) {
        this.rest2models = instance;
        this.uri = null;
        this.req = req;
        this.req.request = this;
        this.targetModelName = null;
        this.parts = null;
        this.data = null;
        this.include = [];
        this.queryConstraints = {};
        this.offset = null;
        this.limit = this.rest2models.pageSize || 100;
        this.order = [['id', 'asc']];
        this.accessLevel = null;
        this.operation = null;

        this._parse();
    }
    process() {
        var defer = q.defer();

        if (this.parts.length == 0) {
            defer.reject(new Error(404));
            return defer.promise;
        }

        // Check endpoint for update operation
        if (this.operation == Request.OPERATION.UPDATE || this.operation == Request.OPERATION.DELETE) {
            if (typeof this.parts.slice(-1).pop().id === 'undefined') {
                defer.reject(new Error(405));
                return defer.promise;
            }
        }

        // Check endpoint for create operation
        if (this.operation == Request.OPERATION.CREATE) {
            if (typeof this.parts.slice(-1).pop().id !== 'undefined') {
                defer.reject(new Error(405));
                return defer.promise;
            }
        }

        this.targetModelName = this.parts.slice(-1).pop().model;

        return this.rest2models.strategy
            .prepare(this)
            .then(this._bindData.bind(this));
    }
    finalize() {
        var defer = q.defer(),
            promise = defer.promise;

        if (!this.targetModelName) {
            defer.reject(new Error(404));
            return promise;
        }

        if (this.operation == Request.OPERATION.READ) {
            defer.resolve(this.data);
            return promise;
        }

        if (this.operation == Request.OPERATION.DELETE) {
            if (!this.data) {
                defer.reject(new Error(404));
                return promise;
            }
            return this.rest2models.strategy
                .delete(this)
                .then(this._bindData.bind(this));
        }

        if (this.operation == Request.OPERATION.UPDATE) {
            if (!this.data) {
                defer.reject(new Error(404));
                return promise;
            }
            return this.rest2models.strategy
                .update(this)
                .then(this._bindData.bind(this));
        }

        if (this.operation == Request.OPERATION.CREATE)
            return this.rest2models.strategy
                .create(this)
                .then(this._bindData.bind(this));
    }
    isIncluded(path) {
        if (typeof path === 'string') {
            path = path.split('.');
        }

        path = path.map(function (item) { return item.toLowerCase(); });

        return !!this.include.find(function (item) {
            var compared = false;

            path.forEach(function (part, index) {
                if (item[index] == '*' || part == item[index]) {
                    compared = true;
                }
                else {
                    compared = false;
                    return false;
                }
            });

            return compared;
        });
    }
    _bindData(data) {

        if (data && data.hasOwnProperty('count') && data.hasOwnProperty('rows')) {
            this.countAll = data.count;
            data = data.rows;
        }

        this.data = data;
        //this.req[utils.decapitalize(this.targetModelName)] = this.data;
        return data;
    }
    _parse() {
        var testRegexp = /^(\/[a-z0-9_-]+\/\d+)*(\/[a-z0-9_-]+)?(\?=)?/i,
            partRegexp = /\/([a-z0-9_-]+)(\/?(\d+))?/gi,
            parts = [],
            match;

        this.uri = this.rest2models.options.baseUrl != '/' ?
            this.req.url.replace(new RegExp('^' + this.rest2models.options.baseUrl), '') : this.req.url;

        if (!testRegexp.test(this.uri)) {
            this.parts = [];
            return;
        }

        // Parse uri
        while ((match = partRegexp.exec(this.uri)) !== null) {
            if (match.index === partRegexp.lastIndex) {
                partRegexp.lastIndex++;
            }

            var modelName = this.rest2models.strategy.getModelName(match[1]);

            if (!modelName) {
                throw new Error(404, 'Not found');
            }

            var obj = {
                model: modelName
            };

            if (typeof match[3] !== 'null') {
                obj.id = match[3];
            }

            parts.push(obj);
        }

        this.parts = parts;

        if (this.parts.length == 0) {
            return;
        }

        // Set operation type and access level
        switch (this.req.method.toUpperCase()) {
            case 'GET':
                this.accessLevel = Request.ACCESSLEVEL.READ;
                this.operation = Request.OPERATION.READ;
                break;
            case 'POST':
                this.accessLevel = Request.ACCESSLEVEL.WRITE;
                this.operation = Request.OPERATION.CREATE;
                break;
            case 'PUT':
                this.accessLevel = Request.ACCESSLEVEL.WRITE;
                this.operation = Request.OPERATION.UPDATE;
                break;
            case 'DELETE':
                this.accessLevel = Request.ACCESSLEVEL.WRITE;
                this.operation = Request.OPERATION.DELETE;
                break;
        }

        debug(JSON.stringify(parts));

        this._parseQueryConstraints();

        this._parseLimitAndOffset();

        this._parseInclude();

        this._parseAttributes();

        if (this.req.query.order) {
            this.order = JSON.parse(this.req.query.order);
        }
        if (this.req.query.attributes) {
            this.selectAttrs = JSON.parse(this.req.query.attributes);
        }
    }
    _parseAttributes() {
        if (this.operation == Request.OPERATION.READ) {
            this.attributes = null;
            return;
        }

        var part = this.parts.slice(-1).pop();
        var modelAttributes = this.rest2models.strategy.getAttributes(part.model);
        var params = extend({}, this.req.body);
        var parentPart = this.parts.slice(-2, -1).pop();

        Object.keys(params).forEach(function (key) {
            params[key] = this._normalizeParameter(params[key]);
        }, this);

        if (parentPart && this.operation == Request.OPERATION.CREATE) {
            params[utils.decapitalize(parentPart.model)] = parentPart.id;
        }

        Object.keys(params).slice().forEach(function (key) {
            var attr = modelAttributes.find(function (attr) {
                return key == attr.name || key == utils.decapitalize(attr.model);
            }, this);

            if (attr) {
                attr.value = params[key];
                params[key] = attr;
            }
            else {
                delete params[key];
            }
        }, this);

        var userAttr = modelAttributes.find(function (attr) {
            return utils.decapitalize(attr.model) == 'user';
        });

        /*if(userAttr && this.operation == Request.OPERATION.CREATE) {
            if(!params[utils.decapitalize(userAttr.model)]) {
                userAttr.value = this.req.user ? this.req.user.id : undefined;
                params[utils.decapitalize(userAttr.model)] = userAttr;
            }
        }*/
        this.attributes = params;
    }
    _parseQueryConstraints() {
        if (this.operation != Request.OPERATION.READ || !this.req.query.where) {
            this.queryConstraints = {};
            return;
        }
        var params;

        try {
            params = JSON.parse(this.req.query.where);
        }
        catch (e) {
            throw new Error(400, e.message);
        }

        if (typeof params != 'object' || Array.isArray(params))
            throw new Error(400);

        var part = this.parts.slice(-1).pop();
        var modelAttributes = this.rest2models.strategy.getAttributes(part.model);

        Object.keys(params).forEach(function (key) {
            params[key] = this._normalizeParameter(params[key]);
        }, this);

        Object.keys(params).slice().forEach(function (key) {
            var attr = modelAttributes.find(function (attr) {
                return key == attr.name || key == utils.decapitalize(attr.model);
            }, this);

            if (attr) {
                attr.value = params[key];
                params[key] = attr;
            }
            else {
                // TODO Реализовать нормальную работу с операторами $or и $and
                if (key != "$or" && key != "$and")
                    delete params[key];
            }
        }, this);

        this.queryConstraints = params;
    }
    _parseLimitAndOffset() {
        if (this.operation == Request.OPERATION.READ) {
            this.offset = parseInt(this.req.query.offset || 0);
            this.limit = parseInt(this.req.query.limit || this.limit);
        }
    }
    _parseInclude() {
        if (this.req.query.include) {
            this.include = this.req.query.include
                .toLowerCase()
                .replace(/^all$/, '*')
                .split(',')
                .map(function (item) {
                    return item.trim().split('.').map(function (item) {
                        return item.trim();
                    });
                });
        }
    }
    _normalizeParameter(value) {
        if (['number', 'string', 'boolean', 'undefined'].indexOf(typeof value) != -1)
            return value;

        if (value && typeof value === 'object') {
            if (value.__type === 'Pointer' || value.__type == 'Object')
                return value.id;

            if (value.__type == 'Date')
                return value.value;

            Object.keys(value).forEach(function (key) {
                value[key] = this._normalizeParameter(value[key]);
            }, this);

            return value;
        }

        if (Array.isArray(value))
            return value.map(function (value) { return this._normalizeParameter(value); }, this);

        return null;
    }
}











Request.__proto__.ACCESSLEVEL = Object.freeze({ READ: 1, WRITE: 2 });
Request.__proto__.OPERATION   = Object.freeze({ READ: 1, UPDATE: 2, CREATE: 3, DELETE: 4 });

module.exports = Request;