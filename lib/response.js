var Error = require('./error'),
    extend = require('node.extend'),
    querystring = require('querystring');

var Response = function(instance, request, res) {
    this.rest2models = instance;
    this.request = request;
    this.res = res;
    this._process();
}

Response.prototype._process = function() {
    if(! this.request.data)
        throw new Error(404);

    this.request.data = this._normalize(this.request.data);

    if(Array.isArray(this.request.data)) {
        this.request.data = this._wrapList(this.request.data);
    }

    this.res.status(200).json(this.request.data);
}

Response.prototype._normalize = function(data) {
    if(Array.isArray(data)) {
        return data.map(this._normalize, this);
    } else {
        return this.rest2models.strategy.normalize(data, this.request);
    }
}

Response.prototype._wrapList = function(data) {
    var next = null,
        prev = null,
        base = this.request.req.protocol + '://' + this.request.req.headers.host + this.request.req.url.replace(/\/?\?.*/, '?');

    var query = extend({}, this.request.req.query);

    if(this.request.countAll > this.request.offset + this.request.limit) {
        query.offset = this.request.offset + this.request.limit;
        query.limit = this.request.limit;
        next = base + querystring.stringify(query);
    }

    if(this.request.offset > 0) {
        query.offset = Math.max(0, this.request.offset - this.request.limit);
        query.limit = this.request.limit;
        prev = base + querystring.stringify(query);
    }

    var wrapper = {
        __type: 'List',
        offset: this.request.offset,
        limit: Math.min(this.request.limit, data.length),
        count: this.request.countAll,
        previous: prev,
        next: next,
        results: data
    }

    return wrapper;
}

module.exports = Response;