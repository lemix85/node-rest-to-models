var q = require('q'),
    extend = require('node.extend'),
    Strategy = require('./strategy').Strategy,
    util = require('util'),
    utils = require('../utils'),
    attr = require('../attributes'),
    ValueAttribute = attr.ValueAttribute,
    RelationAttribute = attr.RelationAttribute,
    error = require('../error'),
    debug = require('debug')('rest2models');

/**
 * Create Sequelize strategy instance
 * @param sequelize
 * @constructor
 */
class SequelizeStrategy {
    constructor(sequelize) {
        this.sequelize = sequelize;

        Strategy.call(this);
    }
    /**
     * Prepare and find objects by parts of path
     * @param request {Request}
     * @returns {*}
     */
    async prepare(request) {
        var options = {},
            cursor = options,
            identifiedParts,
            lastPart = request.parts.slice(-1).pop(),
            promise;

        identifiedParts = request.parts.filter(function (item) { return typeof item.id !== 'undefined'; });

        if (identifiedParts.length > 0) {
            this._loadAssociations(options, identifiedParts.slice(-1).pop(), identifiedParts.slice(0, -1), request.req.user);

            options.where = {
                id: identifiedParts.slice(-1).pop().id
            };

            promise = this._scoped(identifiedParts.slice(-1).pop().model, options, request.req.user).findOne(options);
        }

        if (typeof lastPart.id === 'undefined') {

            const options = { where: {} };
            this._loadAssociations(options, lastPart, request.parts.slice(0, -1), request.req.user);

            if (request.selectAttrs) {
                options.attributes = request.selectAttrs;
            }

            if (request.queryConstraints) {

                Object.keys(request.queryConstraints).forEach(function (key) {
                    var attr = request.queryConstraints[key];

                    if (attr.name)
                        options.where[attr.name] = attr.value;
                    else
                        options.where[key] = attr;

                });
            }

            options.offset = request.offset;
            options.limit = request.limit;
            options.order = request.order;

            if (promise) {
                const data = await promise;

                if (!data) {
                    return null;
                }
            }
            
            const countScopeOptions = {...options};
            const findScopeOptions = {...options};
            
            const count = await this._scoped(lastPart.model, countScopeOptions, request.req.user).count({ 
                ...countScopeOptions, /*, distinct: true, col: lastPart.model + '->id' */
                attributes: [
                    [
                        this.sequelize.fn(
                            'COUNT', 
                            this.sequelize.fn('DISTINCT', this.sequelize.col(lastPart.model + '.id')), 
                        ),
                        'count'
                    ]
                ]
            });
            const rows = await this._scoped(lastPart.model, findScopeOptions, request.req.user).findAll(findScopeOptions);

            return {
                count, 
                rows
            }
        }

        if(promise) {
            return await promise;
        }
    }
    /**
     *
     * @param modelName
     * *returns {Array}
     */
    getAttributes(modelName) {
        var model = this.sequelize.models[modelName];

        if (!model)
            return [];

        var attributes = [];

        Object.keys(model.rawAttributes).forEach(function (key) {
            var association;
            // Load belongsTo association attributes
            if (association = this._findAssociationByFK(model, key)) {
                attributes.push(new attr.RelationAttribute(key,
                    undefined,
                    association.as || association.target.name,
                    undefined,
                    association.associationType,
                    association)
                );
            }
            else {
                attributes.push(new attr.ValueAttribute(key, undefined, model.rawAttributes[key]));
            }
        }, this);

        // Load belongsToMany, hasMany association attributes
        Object.keys(model.associations).forEach(function (key) {
            var association = model.associations[key];
            if(Object.keys(model.rawAttributes).indexOf(association.identifier) === -1) {
                attributes.push(new attr.RelationAttribute(key,
                    undefined,
                    association.as || association.target.name,
                    undefined,
                    association.associationType,
                    association)
                );
            }
        });

        /*Object.keys(model.associations).forEach(function (key) {
            if(model.associations[key].associationType == 'BelongsToMany') {
                attributes.push(new attr.ValueAttribute(key, undefined, model.associations[key]));
            }
        });*/

        return attributes;
    }
    /**
     * Delete specified object
     * @param request
     */
    delete(request) {
        request.data.__isApiRequest = true;
        request.data.dataValues.__requestingUser = request.req.user;

        return request.data.destroy().then(function (data) {
            return request.data;
        });
    }
    /**
     * Update specified object
     * @param request
     */
    async update(request) {
        var attributes = {};

        Object.keys(request.attributes).forEach(function (key) {
            var attr = request.attributes[key];
            attributes[attr.name] = attr.value;
        });

        await this.appendAssociationsAttributes(request.data, attributes, attributes);

        request.data.__isApiRequest = true;
        request.data.dataValues.__requestingUser = request.req.user;
        return request.data.update(attributes).then(function (data) {
            return data;
        });
    }
    /**
     * Create new object
     * @param request
     * @returns {*}
     */
    async create(request) {
        var current = request.parts.slice(-1).pop(),
            model = this.sequelize.models[current.model],
            attributes = {};

        Object.keys(request.attributes).forEach(function (key) {
            var attr = request.attributes[key];
            attributes[attr.name] = attr.value;
        });

        model = model.build(attributes);

        await this.appendAssociationsAttributes(model, attributes, [model, model.dataValues]);

        model.__isApiRequest = true;
        model.dataValues.__requestingUser = request.req.user;

        const savedWithId = await model.save();
        current.id = savedWithId.id;

        return savedWithId;
    }


    async appendAssociationsAttributes(model, dataAttributes, target) {
        const modelAttributes = this.getAttributes(model.constructor.name);
        const relations = modelAttributes.filter(value => value instanceof attr.RelationAttribute);

        for(const relation of relations) {
            const association = relation.source;
            
            if(!dataAttributes[association.as]) {
                continue;
            }

            if(association.associationType === 'BelongsToMany'
                && Array.isArray(dataAttributes[association.as])) {
                const modelCtr = association.target;

                const data = await modelCtr.findAll({
                    where: { 
                        id:  dataAttributes[association.as].map(value => {
                            if(typeof value === 'number') {
                                return value;
                            } else {
                                return value.id
                            }
                        }).filter(value => !!value)
                    }
                });

                if(Array.isArray(target)) {
                    for(const t of target) {
                        t[relation.name] = data;
                    }
                } else {
                    target[relation.name] = data;
                }
            }
        }
    }

    /**
     * Normalize data object
     * @param data
     * @param request
     * @param parent
     */
    normalize(data, request, parent) {
        data.dataValues.__type = 'Object';
        data.dataValues.className = utils.capitalize(data.constructor.name);
        delete data.dataValues.__requestingUser;

        const newData = data.get();

        Object.keys(data.dataValues).slice(0).forEach(function (key) {
            // If field is foreignKey
            if (this._findAssociationByFK(data.constructor.name, key)) {

                var association = this._findAssociationByFK(data.constructor.name, key),
                    field = association.as || association.target.name,
                    decapitalizedField = utils.decapitalize(field),
                    capitalizedField = utils.capitalize(field),
                    path = (parent ? parent + '.' : '') + field;

                if (data.dataValues[key]) {
                    if (!data.dataValues.hasOwnProperty(field) || !request.isIncluded(path)) {
                        if(Array.isArray(data.dataValues[field])) {
                            newData[field] = data.dataValues[field].map(item => {
                                //return this.normalize(item, request, path);
                                return {
                                    __type: 'Pointer',
                                    className: capitalizedField,
                                    id: typeof item === 'number' ? item : item.id
                                }
                            });
                        } else {
                            newData[field] = {
                                __type: 'Pointer',
                                className: capitalizedField,
                                id: data.dataValues[key]
                            };
                        }
                    }
                    else {
                        if(Array.isArray(data.dataValues[field])) {
                            newData[field] = data.dataValues[field].map(item => {
                                return this.normalize(item, request, path);
                            })
                        } else {
                            newData[field] = this.normalize(data.dataValues[field], request, path);
                        }
                    }
                }

                if (decapitalizedField != field) {
                    newData[decapitalizedField] = newData[field];
                    newData[field] = undefined;
                    delete newData[field];
                }

                if(field != key) {
                    newData[key] = undefined;
                    delete newData[key];
                }
            }
            else if (Array.isArray(data[key])) {
                newData[key] = data[key].map(function (item) {
                    if (item && item.constructor && this.sequelize.models[item.constructor.name]) {
                        return this.normalize(item, request);
                    } 
                }.bind(this)).filter(value => !!value);
            }
            // If field is Date
            else if (data.dataValues[key] instanceof Date) {
                if (!isNaN(data.dataValues[key])) {
                    var date = {
                        __type: 'Date',
                        value: data.dataValues[key].toISOString(),
                        locale: data.dataValues[key].toLocaleString()
                    };
                    newData[key] = date;
                }
                else {
                    newData[key] = null;
                }
            } else {
                //newData[key] = data.dataValues[key];
            }
        }, this);

        return newData;
    }
    /**
     * Insensitive finding model
     * @param name
     * @returns {String}
     * @private
     */
    getModelName(name) {
        var model;

        Object.keys(this.sequelize.models).forEach(function (key) {
            if (key.toLowerCase() == name.toLowerCase()) {
                model = this.sequelize.models[key];
                return false;
            }
        }, this);

        return model ? model.name : undefined;
    }
    /**
     * Find BelongsTo association in source model by foreign key
     * @param sourceModel {Sequelize.Model}
     * @param key {String}
     * @returns {Association}
     * @private
     */
    _findAssociationByFK(sourceModel, key) {
        if (!this._foreignKeys) {
            this._foreignKeys = {};
        }

        if (typeof sourceModel === 'string') {
            sourceModel = this.sequelize.models[sourceModel];
        }

        if (!this._foreignKeys[sourceModel.name]) {
            this._foreignKeys[sourceModel.name] = {};

            Object.keys(sourceModel.associations).forEach(function (key) {
                var item = sourceModel.associations[key];

                if (item.associationType == 'BelongsTo') {
                    this._foreignKeys[sourceModel.name][item.identifier] = item;
                } else if(item.associationType == 'BelongsToMany') {
                    this._foreignKeys[sourceModel.name][item.as] = item;
                } else if(item.associationType == 'HasMany') {
                    this._foreignKeys[sourceModel.name][item.as] = item;
                }
            }, this);
        }

        return this._foreignKeys[sourceModel.name][key];
    }
    /**
     * Load associations
     * @param options
     * @param part
     * @param parents
     * @param request
     * @private
     */
    _loadAssociations(options, part, parents, user, level) {
        var model = this.sequelize.models[part.model],
            parent = parents.slice(-1).pop();

        level = level || 0;
        options = options || {};

        if (level >= 3)
            return;

        Object.keys(model.associations).forEach(function (key) {
            var association = model.associations[key];

            if (association.associationType == 'BelongsTo') {
                if (!options.include) {
                    options.include = [];
                }

                if (parent && association.target.name == parent.model) {
                    var constraints = {
                        where: {
                            id: parent.id
                        }
                    };
                    var scopedModel = this._scoped(association.target, constraints, user, association);

                    options.include.push(extend({
                        model: scopedModel,
                        as: association.as
                    }, constraints));

                }
                else {
                    options.include.push({
                        model: this._scoped(association.target, null, user, association),
                        as: association.as || association.target.name,
                        required: options.required === false ? false : !association.source.tableAttributes[association.foreignKey].allowNull
                    });
                }

                this._loadAssociations(options.include.slice(-1).pop(), { model: association.target.name }, parents.slice(0, -1), user, level + 1);
            }
        }, this);

        return options;
    }
    /**
     * Apply accessLevel scope to model and modify option object
     * @param model
     * @param options
     * @param request
     * @returns {*}
     * @private
     */
    _scoped(model, options, user, association) {
        model = typeof model === 'string' ? this.sequelize.models[model] : model;
        options = options || {};

        if (model.options.scopes && model.options.scopes.hasOwnProperty('accessLevel')) {
            var scopeOptions = extend({}, options);
            delete scopeOptions.order;
            options.where = options.attributes = options.include = undefined;
            delete options.where;
            delete options.attributes;
            delete options.include;
            const scope = model.scope({ method: ['accessLevel', user, scopeOptions, association] });
            return scope;
        }

        return model;
    }
}

module.exports = SequelizeStrategy;
util.inherits(SequelizeStrategy, Strategy);