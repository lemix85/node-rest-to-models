var q = require('q');

module.exports.promiseChainer = function(arr) {
    var defer = q.defer(),
        index = -1,
        next,
        ctx = this;

    next = function() {
        index++;
        if(index < arr.length) {
            try {
                arr[index].apply(ctx).then(function () {
                    next.apply(ctx);
                }).catch(function (err) {
                    defer.reject(err);
                });
            } catch(err) {
                debug(JSON.stringify(err));
                defer.reject(err);
            }
        } else {
            defer.resolve();
        }
    };

    next.apply(ctx);

    return defer.promise;
};

module.exports.capitalize = function(string) {
    string = string || '';
    string = string.trim();

    if (string[0]) {
        string = string[0].toUpperCase() + string.substr(1);
    }

    return string
}

module.exports.decapitalize = function(string) {
    string = string || '';
    string = string.trim();

    if (string[0]) {
        string = string[0].toLowerCase() + string.substr(1);
    }

    return string
}