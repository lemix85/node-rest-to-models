const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const app = express();
const multipart = require('connect-multiparty');
const rest2models = require('../index');
const SequelizeStrategy = rest2models.SequelizeStrategy;
const debug = require('debug')('rest2models');
const { Sequelize, Op } = require("sequelize");
const sequelize = new Sequelize('test_database', 'user', 'password', {
    host: 'localhost',
    port: '32772',
    dialect: 'postgres',
    sync: { force: true },
    logging: false,
    operatorsAliases: {
        $or: Op.or,
        $like: Op.like,
        $iLike: Op.iLike,
        $and: Op.and,
        $in: Op.in,
        $is: Op.is
    }
});
const Post = require('./models/post')(sequelize, Sequelize);
const Comment = require('./models/comment')(sequelize, Sequelize);
const User = require('./models/user')(sequelize, Sequelize);
const Role = require('./models/role')(sequelize, Sequelize);
const CreditCard = require('./models/credit-card')(sequelize, Sequelize);

User.hasMany(Role, { foreignKey: 'userId' });
User.hasMany(Comment, { foreignKey: 'userId' });
User.hasMany(Post, { foreignKey: 'userId' });
User.hasMany(CreditCard, { foreignKey: 'userId' });
Post.hasMany(Comment, { foreignKey: 'postId' });
Post.belongsTo(User, { foreignKey: 'userId' });
CreditCard.belongsTo(User, { foreignKey: 'userId' });
Comment.belongsTo(User, { foreignKey: 'userId' });
Role.belongsTo(User, { foreignKey: 'userId' });
Comment.belongsTo(Post, { foreignKey: 'postId' });

//console.log(Comment.associations);
app.sequlize = sequelize;
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(multipart({uploadDir: './tmp'}));

app.use(function (req, res, next) {
    if(req.query.uid) {
        return User.findByPk(req.query.uid).then(function(user) {
            req.user = user;
            next();
        });
    }
    next();
});

rest2models(app, new SequelizeStrategy(sequelize), { baseUrl: '/' });

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers
app.use(function (err, req, res, next) {
    console.log(err);
    var status = err.status || 500;
    if(status == 500) debug(JSON.stringify(err));
    res.status(status).json(err);
});

module.exports = app;