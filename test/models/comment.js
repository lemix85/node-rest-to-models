module.exports = function(sequelize, Sequelize) {
    var Comment = sequelize.define('Comment', {
        postId: {
            type: Sequelize.INTEGER,
            references: {
                model: sequelize.models.Post,
                key: 'id',
                deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
            }
        },
        userId: {
            type: Sequelize.INTEGER,
            references: {
                model: sequelize.models.User,
                key: 'id',
                deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
            }
        },
        text: Sequelize.TEXT
    });

    return Comment;
}