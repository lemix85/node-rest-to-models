module.exports = function(sequelize, Sequelize) {
    var CreditCard = sequelize.define('CreditCard', {
        number: Sequelize.STRING,
        userId: {
            type: Sequelize.INTEGER,
            references: {
                model: sequelize.models.User,
                key: 'id',
                deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
            }
        },
        text: Sequelize.TEXT
    }, {
        scopes: {
            accessLevel: function(user, options) {
                options.where = options.where || {};
                options.where.userId = user ? user.id : -1;
                return options;
            }
        }
    });

    return CreditCard;
}