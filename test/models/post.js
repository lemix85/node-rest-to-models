module.exports = function(sequelize, Sequelize) {
    var Post = sequelize.define('Post', {
        title: Sequelize.STRING,
        text: Sequelize.TEXT,
        userId: {
            type: Sequelize.INTEGER,
            references: {
                model: sequelize.models.User,
                key: 'id',
                deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
            }
        }
    });

    return Post;
};