module.exports = function(sequelize, Sequelize) {
    var Role = sequelize.define('Role', {
        userId: {
            type: Sequelize.INTEGER,
            references: {
                model: sequelize.models.User,
                key: 'id',
                deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
            }
        },
        role: Sequelize.STRING
    });

    return Role;
}