module.exports = function(sequelize, Sequelize) {
    var User = sequelize.define('User', {
        name: Sequelize.STRING
    });

    return User;
};