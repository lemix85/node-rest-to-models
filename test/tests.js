var expect = require('Chai').expect;
var request = require('request-promise');
var app = require('./app.js');

describe('CRUD-operation', function () {
    var server;

    before(function (done) {
        server = app.listen(8000);
        app.sequlize.sync().then(function() {
            return app.sequlize.models.User.create({ name: 'user1' })
        }).then(function() {
            return app.sequlize.models.User.create({ name: 'user2' })
        }).then(function() {
            return app.sequlize.models.User.create({ name: 'user3' })
        }).then(function() {
            return app.sequlize.models.Post.create({ title: 'post1', text: 'post1_text', userId: 1 })
        }).then(function() {
            return app.sequlize.models.Post.create({ title: 'post2', text: 'post2_text', userId: 2 })
        }).then(function() {
            return app.sequlize.models.Post.create({ title: 'post3', text: 'post3_text', userId: 3 })
        }).then(function() {
            return app.sequlize.models.Comment.create({ postId: 1, text: 'comment1_text', userId: 1 });
        }).then(function() {
            return app.sequlize.models.Comment.create({ postId: 1, text: 'comment2_text', userId: 2 });
        }).then(function() {
            return app.sequlize.models.Comment.create({ postId: 1, text: 'comment3_text', userId: 3 });
        }).then(function() {
            return app.sequlize.models.Comment.create({ postId: 2, text: 'comment4_text', userId: 1 });
        }).then(function() {
            return app.sequlize.models.Comment.create({ postId: 2, text: 'comment5_text', userId: 2 });
        }).then(function() {
            return app.sequlize.models.Comment.create({ postId: 2, text: 'comment6_text', userId: 3 });
        }).then(function() {
            return app.sequlize.models.Comment.create({ postId: 3, text: 'comment7_text', userId: 1 });
        }).then(function() {
            return app.sequlize.models.Comment.create({ postId: 3, text: 'comment8_text', userId: 2 });
        }).then(function() {
            return app.sequlize.models.Comment.create({ postId: 3, text: 'comment9_text', userId: 3 });
        }).then(function() {
            return app.sequlize.models.CreditCard.create({ number: '0000000000000000', userId: 1 })
        }).then(function() {
            return app.sequlize.models.CreditCard.create({ number: '0000000000000001', userId: 2 })
        }).then(function() {
            return app.sequlize.models.CreditCard.create({ number: '0000000000000002', userId: 3 })
        }).finally(function() { done(); });
    });

    it('empty path should return 404', function (done) {
        var status=200;
        request.get('http://localhost:8000/').catch(function(err) {
            status = err.response.statusCode;
        }).then(function() {
            expect(status).to.equal(404);
            done();
        }).catch(function(err) {
            done(err);
        });
    });

    it('out of range id should return 404', function (done) {
        var status1=200, status2=200;
        request.get('http://localhost:8000/post/999').catch(function(err) {
            status1 = err.response.statusCode;
        }).then(function() {
            return request.get('http://localhost:8000/post/1/comment/999').catch(function(err) {
                status2 = err.response.statusCode;
            });
        }).then(function() {
            expect(status1).to.equal(404);
            expect(status2).to.equal(404);
            done();
        }).catch(function(err) {
            done(err);
        });
    });

    it('unknown model should return 404', function (done) {
        var status1=200, status2=200;
        request.get('http://localhost:8000/unknown').catch(function(err) {
            status1 = err.response.statusCode;
        }).then(function() {
            return request.get('http://localhost:8000/post/1/unknown').catch(function(err) {
                status2 = err.response.statusCode;
            });
        }).then(function() {
            expect(status1).to.equal(404);
            expect(status2).to.equal(404);
            done();
        }).catch(function(err) {
            done(err);
        });
    });

    describe('READ', function() {
        it('should return List of all objects', function (done) {
            request.get('http://localhost:8000/post').then(function (response) {
                var res = JSON.parse(response);
                expect(res).to.have.deep.property('results.length', 3);
                expect(res).to.have.deep.property('results.0.__type', 'Object');
                expect(res).to.have.deep.property('results.0.className', 'Post');
                expect(res).to.have.deep.property('results.0.id', 1);
                expect(res).to.have.deep.property('results.0.title', 'post1');
                expect(res).to.have.deep.property('results.0.text', 'post1_text');
            }).then(function () {
                return request.get('http://localhost:8000/post/1/comment').then(function (response) {
                    var res = JSON.parse(response);
                    expect(res).to.have.deep.property('results.length', 3);
                    expect(res).to.have.deep.property('results.0.__type', 'Object');
                    expect(res).to.have.deep.property('results.0.className', 'Comment');
                    expect(res).to.have.deep.property('results.0.id');
                    expect(res).to.have.deep.property('results.0.text');
                    expect(res).to.have.deep.property('results.0.post.__type', 'Pointer');
                    expect(res).to.have.deep.property('results.0.post.className', 'Post');
                    expect(res).to.have.deep.property('results.0.post.id', 1);
                });
            }).then(function () {
                done();
            }).catch(function (err) {
                done(err);
            });
        });

        it('by id should return specified object', function (done) {
            request.get('http://localhost:8000/post/1').then(function (response) {
                var res = JSON.parse(response);
                expect(res).to.have.deep.property('__type', 'Object');
                expect(res).to.have.deep.property('className', 'Post');
                expect(res).to.have.deep.property('id', 1);
                expect(res).to.have.deep.property('title', 'post1');
                expect(res).to.have.deep.property('text', 'post1_text');
            }).then(function () {
                return request.get('http://localhost:8000/post/1/comment/1').then(function (response) {
                    var res = JSON.parse(response);
                    expect(res).to.have.deep.property('__type', 'Object');
                    expect(res).to.have.deep.property('className', 'Comment');
                    expect(res).to.have.deep.property('id', 1);
                    expect(res).to.have.deep.property('text', 'comment1_text');
                    expect(res).to.have.deep.property('post.__type', 'Pointer');
                    expect(res).to.have.deep.property('post.className', 'Post');
                    expect(res).to.have.deep.property('post.id', 1);
                });
            }).then(function () {
                done();
            }).catch(function (err) {
                done(err);
            });
        });

        describe('from accessLevel-scoped model', function() {
            it('from unauthorized user should return empty List', function (done) {
                request.get('http://localhost:8000/creditcard').then(function (response) {
                    var res = JSON.parse(response);
                    expect(res).to.have.deep.property('__type', 'List');
                    expect(res).to.have.deep.property('results.length', 0);
                    done();
                }).catch(function (err) {
                    done(err);
                });
            });

            it('access to another object should return 404', function (done) {
                var status=200;
                request.get('http://localhost:8000/creditcard/2?uid=1').catch(function(err) {
                    status = err.response.statusCode;
                }).then(function() {
                    expect(status).to.equal(404);
                    done();
                }).catch(function(err) {
                    done(err);
                });
            });

            it('should return List scoped objects', function (done) {
                request.get('http://localhost:8000/creditcard?uid=1').then(function (response) {
                    var res = JSON.parse(response);
                    expect(res).to.have.deep.property('__type', 'List');
                    expect(res).to.have.deep.property('results.length', 1);
                    expect(res).to.have.deep.property('results.0.__type', 'Object');
                    expect(res).to.have.deep.property('results.0.className', 'CreditCard');
                    expect(res).to.have.deep.property('results.0.user.id', 1);
                    done();
                }).catch(function (err) {
                    done(err);
                });
            });
        });

        describe('list', function() {
            it('should return List object with specified offset and limit', function (done) {
                request.get('http://localhost:8000/comment?offset=0&limit=2').then(function (response) {
                    var res = JSON.parse(response);
                    expect(res).to.have.deep.property('__type', 'List');
                    expect(res).to.have.deep.property('results');
                    expect(res).to.have.deep.property('results.length', 2);
                    expect(res).to.have.deep.property('offset', 0);
                    expect(res).to.have.deep.property('limit', 2);
                    expect(res).to.have.deep.property('count');
                    expect(res.previous).to.be.a('null');
                    expect(res).to.have.deep.property('next', 'http://localhost:8000/comment?offset=2&limit=2');
                    return request.get(res.next);
                }).then(function (response) {
                    var res = JSON.parse(response);
                    expect(res).to.have.deep.property('__type', 'List');
                    expect(res).to.have.deep.property('results');
                    expect(res).to.have.deep.property('results.length', 2);
                    expect(res).to.have.deep.property('offset', 2);
                    expect(res).to.have.deep.property('limit', 2);
                    expect(res).to.have.deep.property('count');
                    expect(res).to.have.deep.property('previous', 'http://localhost:8000/comment?offset=0&limit=2');
                    expect(res).to.have.deep.property('next', 'http://localhost:8000/comment?offset=4&limit=2');
                    done();
                }).catch(function (err) {
                    done(err);
                });
            });
        });

        describe('?order', function() {
            it('should return sorted List objects', function (done) {
                request.get('http://localhost:8000/post?order=[["id","desc"]]').then(function (response) {
                    var res = JSON.parse(response);
                    expect(res).to.have.deep.property('__type', 'List');
                    expect(res).to.have.deep.property('results.length', 3);
                    expect(res).to.have.deep.property('results.0.id', 3);
                    expect(res).to.have.deep.property('results.1.id', 2);
                    expect(res).to.have.deep.property('results.2.id', 1);
                    done();
                }).catch(function (err) {
                    done(err);
                });
            });
        });

        describe('?where', function() {
            it('by operator should return array of condition objects', function (done) {
                request({
                        url: 'http://localhost:8000/post/1/comment',
                        qs: {
                            where: JSON.stringify({
                                text: {
                                    $like: 'comment1_text'
                                }
                            })
                        }
                }).then(function (response) {
                    var res = JSON.parse(response);
                    expect(res).to.have.deep.property('results.length', 1);
                    expect(res).to.have.deep.property('results.0.__type', 'Object');
                    expect(res).to.have.deep.property('results.0.className', 'Comment');
                    expect(res).to.have.deep.property('results.0.text', 'comment1_text');
                    expect(res).to.have.deep.property('results.0.post');
                    expect(res).to.have.deep.property('results.0.post.__type', 'Pointer');
                    expect(res).to.have.deep.property('results.0.post.className', 'Post');
                    expect(res).to.have.deep.property('results.0.post.id', 1);
                    expect(res).to.have.deep.property('results.0.user');
                    expect(res).to.have.deep.property('results.0.user.__type', 'Pointer');
                    expect(res).to.have.deep.property('results.0.user.className', 'User');
                    expect(res).to.have.deep.property('results.0.user.id', 1);
                    done();
                }).catch(function (err) {
                    done(err);
                });
            });

            it('by pointer should return array of condition objects', function (done) {
                request({
                    url: 'http://localhost:8000/comment',
                    qs: {
                        where: JSON.stringify({
                            post: {
                                __type: 'Pointer',
                                className: 'Post',
                                id: 1
                            }
                        })
                    }
                }).then(function (response) {
                    var res = JSON.parse(response);
                    expect(res).to.have.deep.property('results.length', 3);
                    expect(res).to.have.deep.property('results.0.post.id', 1);
                    expect(res).to.have.deep.property('results.1.post.id', 1);
                    expect(res).to.have.deep.property('results.2.post.id', 1);
                    expect(res).to.have.deep.property('results.0.user.id', 1);
                    expect(res).to.have.deep.property('results.1.user.id', 2);
                    expect(res).to.have.deep.property('results.2.user.id', 3);
                    done();
                }).catch(function (err) {
                    done(err);
                });
            });

            it('by operator with pointer should return array of condition objects', function (done) {
                request({
                    url: 'http://localhost:8000/comment',
                    qs: {
                        where: JSON.stringify({
                            post: {
                                $in: [
                                    {
                                        __type: 'Pointer',
                                        className: 'Post',
                                        id: 1
                                    },
                                    {

                                        __type: 'Pointer',
                                        className: 'Post',
                                        id: 2
                                    }
                                ]
                            }
                        })
                    }
                }).then(function (response) {
                    var res = JSON.parse(response);
                    expect(res).to.have.deep.property('results.length', 6);
                    res.results.forEach(function(item) {
                        expect(item.post).to.have.property('id').of.at.least(1);
                        expect(item.post).to.have.property('id').of.at.most(2);
                    });
                    done();
                }).catch(function (err) {
                    done(err);
                });
            });

            it('by iLike operator should return array of condition objects', function (done) {
                request({
                    url: 'http://localhost:8000/post',
                    qs: {
                        where: JSON.stringify({
                            "$or":[
                                {"title":{"$iLike":"%ost2%"}},
                                {"text":{"$iLike":"%t2_te%"}}
                            ]
                        })
                    }
                }).then(function (response) {
                    var res = JSON.parse(response);
                    expect(res).to.have.deep.property('results.length', 1);
                    expect(res).to.have.deep.property('results.0.id', 2);
                    done();
                }).catch(function (err) {
                    done(err);
                });
            });
        });

        describe('?include', function() {

            it('all should return object with all associations', function (done) {
                request.get('http://localhost:8000/post/1/comment/1?include=*').then(function(response) {
                    var res = JSON.parse(response);
                    expect(res).to.have.deep.property('__type', 'Object');
                    expect(res).to.have.deep.property('className', 'Comment');
                    expect(res).to.have.deep.property('id', 1);
                    expect(res).to.have.deep.property('text', 'comment1_text');
                    expect(res).to.have.deep.property('post.__type', 'Object');
                    expect(res).to.have.deep.property('post.className', 'Post');
                    expect(res).to.have.deep.property('post.id', 1);
                    expect(res).to.have.deep.property('post.title', 'post1');
                    expect(res).to.have.deep.property('post.text', 'post1_text');
                    expect(res).to.have.deep.property('user.__type', 'Object');
                    expect(res).to.have.deep.property('user.className', 'User');
                    expect(res).to.have.deep.property('user.id', 1);
                    expect(res).to.have.deep.property('user.name', 'user1');
                    done();
                }).catch(function(err) {
                    done(err);
                });
            });

            it('all should return objects with all associations', function (done) {
                request.get('http://localhost:8000/post/1/comment?include=*').then(function(response) {
                    var res = JSON.parse(response);
                    expect(res).to.have.deep.property('results.0.post.__type', 'Object');
                    expect(res).to.have.deep.property('results.0.post.className', 'Post');
                    expect(res).to.have.deep.property('results.0.post.id', 1);
                    expect(res).to.have.deep.property('results.0.post.title', 'post1');
                    expect(res).to.have.deep.property('results.0.post.text', 'post1_text');
                    expect(res).to.have.deep.property('results.0.user.__type', 'Object');
                    done();
                }).catch(function(err) {
                    done(err);
                });
            });

            it('as classNames separated by commas should return with specified associations', function (done) {
                request.get('http://localhost:8000/post/1/comment/1?include=post').then(function(response) {
                    var res = JSON.parse(response);
                    expect(res).to.have.deep.property('__type', 'Object');
                    expect(res).to.have.deep.property('className', 'Comment');
                    expect(res).to.have.deep.property('id', 1);
                    expect(res).to.have.deep.property('text', 'comment1_text');
                    expect(res).to.have.deep.property('post.__type', 'Object');
                    expect(res).to.have.deep.property('post.className', 'Post');
                    expect(res).to.have.deep.property('post.id', 1);
                    expect(res).to.have.deep.property('post.title', 'post1');
                    expect(res).to.have.deep.property('post.text', 'post1_text');
                    expect(res).to.have.deep.property('user.__type', 'Pointer');
                    expect(res).to.have.deep.property('user.className', 'User');
                    expect(res).to.have.deep.property('user.id', 1);
                    done();
                }).catch(function(err) {
                    done(err);
                });
            });
        })
    });

    describe('UPDATE', function() {

        it('by unspecified id should return 405', function (done) {
            var status = 200;
            request.put('http://localhost:8000/post', {
                form: {
                    title: 'title',
                    text: 'text'
                }
            }).catch(function (err) {
                status = err.response.statusCode;
            }).then(function () {
                expect(status).to.equal(405);
                done();
            }).catch(function (err) {
                done(err);
            });
        });

        it('should return updated object', function (done) {
            request.put('http://localhost:8000/post/1', {
                form: {
                    title: 'post1 updated',
                    text: 'post1_text updated'
                }
            }).then(function (response) {
                var res = JSON.parse(response);
                expect(res).to.have.deep.property('id', 1);
                expect(res).to.have.deep.property('title', 'post1 updated');
                expect(res).to.have.deep.property('text', 'post1_text updated');
            }).then(function () {
                return request.put('http://localhost:8000/post/1/comment/1', {
                    form: {
                        text: 'comment1_text updated'
                    }
                }).then(function (response) {
                    var res = JSON.parse(response);
                    expect(res).to.have.deep.property('id', 1);
                    expect(res).to.have.deep.property('text', 'comment1_text updated');
                    expect(res).to.have.deep.property('post.id', 1);
                    expect(res).to.have.deep.property('post.__type', 'Pointer');
                    expect(res).to.have.deep.property('post.className', 'Post');
                });
            }).then(function () {
                done();
            }).catch(function (err) {
                done(err);
            });
        });

        it('pointer field should return object with updated pointer field', function (done) {
            request.put('http://localhost:8000/comment/1', {
                json: true,
                body: {
                    post: {
                        __type: 'Pointer',
                        className: 'Post',
                        id: 2
                    }
                }
            }).then(function (res) {
                expect(res).to.have.deep.property('id', 1);
                expect(res).to.have.deep.property('post.id', 2);
                expect(res).to.have.deep.property('post.__type', 'Pointer');
                expect(res).to.have.deep.property('post.className', 'Post');
                done();
            }).catch(function (err) {
                done(err);
            });
        });
    });

    describe('CREATE', function() {

        it('when contains id should return 405', function (done) {
            var status = 200;
            request.post('http://localhost:8000/post/1', {
                form: {
                    title: 'title',
                    text: 'text'
                }
            }).catch(function (err) {
                status = err.response.statusCode;
            }).then(function () {
                expect(status).to.equal(405);
                done();
            }).catch(function (err) {
                done(err);
            });
        });

        it('should return new object', function (done) {
            request.post('http://localhost:8000/Post', {
                form: {
                    title: 'post4',
                    text: 'post4_text',
                    user: 1
                }
            }).then(function (response) {
                var res = JSON.parse(response);
                expect(res).to.have.deep.property('id', 4);
                expect(res).to.have.deep.property('title', 'post4');
                expect(res).to.have.deep.property('text', 'post4_text');
                expect(res).to.have.property('user');
                done();
            }).catch(function (err) {
                done(err);
            });
        });

        it('with pointer field should return new object contained pointer field', function (done) {
            request.post('http://localhost:8000/comment', {
                json: true,
                body: {
                    text: 'text',
                    post: {
                        __type: 'Pointer',
                        className: 'Post',
                        id: 2
                    },
                    user: {
                        __type: 'Pointer',
                        className: 'User',
                        id: 1
                    }
                }
            }).then(function (res) {
                expect(res).to.have.deep.property('text', 'text');
                expect(res).to.have.deep.property('post.id', 2);
                expect(res).to.have.deep.property('post.__type', 'Pointer');
                expect(res).to.have.deep.property('post.className', 'Post');
                expect(res).to.have.property('user');
                done();
            }).catch(function (err) {
                done(err);
            });
        });
    });

    describe('DELETE', function() {

        it('by unspecified id should return 405', function (done) {
            var status = 200;
            request.del('http://localhost:8000/post').catch(function (err) {
                status = err.response.statusCode;
            }).then(function () {
                expect(status).to.equal(405);
                done();
            }).catch(function (err) {
                done(err);
            });
        });

        it('should return deleted object', function (done) {
            request.del('http://localhost:8000/post/2').then(function (response) {
                var res = JSON.parse(response);

                expect(res).to.have.deep.property('id', 2);
                expect(res).to.have.deep.property('title', 'post2');
                expect(res).to.have.deep.property('text', 'post2_text');

                var status = 200;
                return request.get('http://localhost:8000/post/2').catch(function(err) {
                    status = err.response.statusCode;
                }).finally(function() {
                    expect(status).to.equal(404);
                    done();
                })
            }).catch(function (err) {
                done(err);
            });
        });
    });

    after(function () {
        server.close();
    });
});